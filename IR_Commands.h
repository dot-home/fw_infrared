//********************************************************************************
/*!
\author     Kraemer E 
\date       01.02.2022

\file       IR_Commands.h
\brief      Holds the information about the available commands
            of the IR

***********************************************************************************/
#ifndef _IR_COMMANDS_H_
#define _IR_COMMANDS_H_
    
#ifdef __cplusplus
extern "C"
{
#endif   

/********************************* includes **********************************/    
#include "stdint.h"

/***************************** defines / macros ******************************/

/****************************** type definitions *****************************/    
typedef struct
{
    uint8_t ucCommandCode;
}tsNEC_Commands;

typedef enum
{
    eNEC_Addr_0 = 0xEA41,
    eNEC_Addr_1 = 0x4DB2,
    eNEC_Addr_2 = 0xBBAA
}teNEC_Address;

#define NEC_PROTOCOL_MAP\
    NEC( eNEC_Cmd_ON    ,   0x15    ,   0x82,   0x62)\
    NEC( eNEC_Cmd_OFF   ,   0x14    ,   0xDC,   0x62)\
    NEC( eNEC_Cmd_Plus  ,   0x12    ,   0xCA,   0x69)\
    NEC( eNEC_Cmd_Minus ,   0x13    ,   0xD2,   0x65)\
    NEC( eNEC_Cmd_Colder,   0x11    ,   0x99,   0x63)\
    NEC( eNEC_Cmd_Warmer,   0x10    ,   0xC1,   0x6A)\
    NEC( eNEC_Cmd_Full  ,   0x48    ,   0xCE,   0x1A)\
    NEC( eNEC_Cmd_Night ,   0x41    ,   0x80,   0x61)\
    NEC( eNEC_Cmd_Flash ,   0xFF    ,   0xFF,   0x6E)\
    NEC( eNEC_Cmd_Color ,   0xFF    ,   0xFF,   0x68)\
    NEC( eNEC_Cmd_Smooth,   0xFF    ,   0xFF,   0x6C)

typedef enum
{
    #ifdef NEC_PROTOCOL_MAP
        #define NEC(NAME, Cmd0, Cmd1, Cmd2) NAME,
            NEC_PROTOCOL_MAP
        #undef NEC
    #endif
    eNEC_Cmd_Invalid
}teNEC_Commands;

/***************************** global variables ******************************/

/************************ externally visible functions ***********************/
teNEC_Commands IR_Commands_GetCommand(uint8_t ucCommand, teNEC_Address eNEC_Address);

#ifdef __cplusplus
}
#endif    

#endif //_IR_COMMANDS_H_

