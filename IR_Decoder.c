//********************************************************************************
/*!
\author     Kraemer E.
\date       01.02.2022

\file       IR_Decoder.c
\brief      Module for decoding the infrared remote control with 
            a NEC-Protocol

***********************************************************************************/

#include "project.h"
#include "stdbool.h"
#include "IR_Decoder.h"
/****************************************** Defines ******************************************************/
#define START_PULS_MIN  8900    //8.9ms
#define START_PULS_MAX  9150    //9.15ms

#define SECOND_PULS_MIN    4400    //4.4ms
#define SECOND_PULS_MAX    4600    //4.6ms

#define SECOND_PULS_REP_MIN 2000    //2ms
#define SECOND_PULS_REP_MAX 2500    //2.5ms

#define PULSE_MIN   520 //520us
#define PULSE_MAX   600 //600us

#define THREE_PULSE_MIN (3* PULSE_MIN)
#define THREE_PULSE_MAX (3* PULSE_MAX)

#define PULSES_PER_BIT  2   //A bit is constructed from two pulse informations
#define INVALID_LEVEL   0xFF
#define SHORT_LEVEL     0
#define LONG_LEVEL      1

#define BITS_PER_DATA   8


typedef enum
{
    eIR_Start_0,
    eIR_Start_1,
    eIR_Addr_Low,
    eIR_Addr_High,
    eIR_Command,
    eIR_InvCommand,
    eIR_Repeat
}teIR_Data_State;

/****************************************** Variables ****************************************************/
static tsIR_Data sIR_Data;

static bool bNewValues = false;

static teIR_Data_State eIR_State;

/****************************************** Function prototypes ******************************************/


/****************************************** local functions *********************************************/
//********************************************************************************
/*!
\author     Kraemer E.
\date       01.02.2022
\brief      Fills the structure of IR-Data with new values
\return     bDataFilledUp - True when a whole messages has been put
                            together.
\param      pucData - Pointer to the data which shall be used
\param      ulPulse_us - The pulse duration in microseconds
***********************************************************************************/
static bool FillData(uint8_t* pucData, uint32_t ulPulse_us)
{    
    bool bDataFilledUp = false;
    static uint8_t ucBitCnt = 0;
    static uint8_t ucPulses = 0;
    static uint8_t ucPulseIdx = 0;
    
    //Check for data 
    if(pucData)
    {
        //First pulse should be a single pulse
        if(PULSE_MIN <= ulPulse_us && ulPulse_us <= PULSE_MAX)
        {
            ucPulses |= SHORT_LEVEL << ucPulseIdx;
            ucPulseIdx++;
        }
        
        //Second pulse should be a three-times sized pulse
        if(THREE_PULSE_MIN <= ulPulse_us && ulPulse_us <= THREE_PULSE_MAX)
        {
            ucPulses |= LONG_LEVEL << ucPulseIdx;
            ucPulseIdx++;
        }
        
        //Check if both pulses have been received
        if(ucPulseIdx >= PULSES_PER_BIT)
        {
            //Logical 0 received
            if(ucPulses == 0)
            {
                *pucData &= ~(1 << ucBitCnt);
            }
            //Logical 1 received
            else
            {
                *pucData |= 1 << ucBitCnt;
            }
            
            //Increment bit counter
            ucBitCnt++;
            
            //Reset pulse index
            ucPulseIdx = 0;
            ucPulses = 0;
        }
        
        //Check if the data has been filled up
        if(ucBitCnt == BITS_PER_DATA)
        {
            ucBitCnt = 0;
            bDataFilledUp = true;
        }
    }
    
    return bDataFilledUp;
}


/****************************************** External visible functiones **********************************/

//********************************************************************************
/*!
\author     Kraemer E.
\date       01.02.2022
\brief      Initializes the decoder module
\return     none
\param      none
***********************************************************************************/
void IR_Decoder_Init(void)
{
    //Set IR-State back to start
    eIR_State = eIR_Start_0;
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       01.02.2022
\brief      Returns the current infrared data within a critical section.
            Avoid change of data during copy.
\return     none
\param      psData - Pointer to the structure where the data shall be hold
***********************************************************************************/
bool IR_Decoder_GetCurrentData(tsIR_Data* psData)
{
    bool bResult = false;
    
    if(bNewValues)
    {
        //Copy values in a critical section
        uint8_t ucIntr = CyEnterCriticalSection();
        memcpy(psData, &sIR_Data, sizeof(tsIR_Data));
        bNewValues = false;
        CyExitCriticalSection(ucIntr);        
        bResult = true;
    }
    
    return bResult;
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       01.02.2022
\brief      Interpretes the captured value of the timer.
\return     none
\param      ulCaptureValue - The capture value between two interrupts.
***********************************************************************************/
void IR_Decoder_InterpreteCaptureValue(uint32_t ulCaptureValue)
{
    //Calculate pulse duration in microseconds
    uint32_t ulPulse_us = (ulCaptureValue * 10) / 8;
        
    switch(eIR_State)
    {
        case eIR_Start_0:
        {
            //Check if start Pulse is around 9ms long
            if(START_PULS_MIN <= ulPulse_us && ulPulse_us <= START_PULS_MAX)
            {
                eIR_State = eIR_Start_1;
            }
            break;
        }
        
        case eIR_Start_1:
        {
            //Check if seconds pulse is around 4.5ms long
            if(SECOND_PULS_MIN <= ulPulse_us && ulPulse_us <= SECOND_PULS_MAX)
            {
                eIR_State = eIR_Addr_Low;
            }
            //When the second pulse is around 2.25ms long than its a repeat code
            else if(SECOND_PULS_REP_MIN <= ulPulse_us && ulPulse_us <= SECOND_PULS_REP_MAX)
            {
                eIR_State = eIR_Repeat;
            }
            //Invalid timing
            else
            {
                eIR_State = eIR_Start_0;
            }
            break;
        }
        
        case eIR_Addr_Low:
        {
            if( FillData(&sIR_Data.ucAddress_Low, ulPulse_us))
            {
                eIR_State = eIR_Addr_High;
            }
            break;
        }
        
        case eIR_Addr_High:
        {
            if( FillData(&sIR_Data.ucAddress_High, ulPulse_us))
            {
                eIR_State = eIR_Command;
            }
            break;
        }
        
        case eIR_Command:
        {
            if( FillData(&sIR_Data.ucCommand, ulPulse_us))
            {
                eIR_State = eIR_InvCommand;
            }            
            break;
        }
        
        case eIR_InvCommand:
        {
            if( FillData(&sIR_Data.ucInvCommand, ulPulse_us))
            {
                eIR_State = eIR_Start_0;               
                bNewValues = true;
            }
            break;
        }
        
        case eIR_Repeat:
        {
            //pulse should be a single pulse
            if(PULSE_MIN <= ulPulse_us && ulPulse_us <= PULSE_MAX)
            { 
               eIR_State = eIR_Start_0;
               bNewValues = true;
            }
            break;
        }
    }
}