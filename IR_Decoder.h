//********************************************************************************
/*!
\author     Kraemer E 
\date       01.02.2022

\file       IR_Decoder.h
\brief      Interface for decoding the infrared data

***********************************************************************************/
#ifndef _IR_DECODER_H_
#define _IR_DECODER_H_
    
#ifdef __cplusplus
extern "C"
{
#endif   

/********************************* includes **********************************/    
#include "stdint.h"
#include "stdbool.h"

/***************************** defines / macros ******************************/

/****************************** type definitions *****************************/    
/// Structure to hold the IR data
typedef struct
{
    uint8_t ucAddress_Low;
    uint8_t ucAddress_High;
    uint8_t ucCommand;
    uint8_t ucInvCommand;
}tsIR_Data;

/***************************** global variables ******************************/

/************************ externally visible functions ***********************/
void IR_Decoder_Init(void);
bool IR_Decoder_GetCurrentData(tsIR_Data* psData);
void IR_Decoder_InterpreteCaptureValue(uint32_t ulCaptureValue);

#ifdef __cplusplus
}
#endif    

#endif //_IR_DECODER_H_
