//********************************************************************************
/*!
\author     Kraemer E.
\date       01.02.2022

\file       IR_Commands.c
\brief      Module to fill up the commands and get the commands
***********************************************************************************/

#include "IR_Commands.h"
#include "BaseTypes.h"
/****************************************** Defines ******************************************************/


/****************************************** Variables ****************************************************/
static const tsNEC_Commands sNEC_CMD_0[] =
{
    #ifdef NEC_PROTOCOL_MAP
        #define NEC(Name, Cmd0, Cmd1, Cmd2) {Cmd0},
            NEC_PROTOCOL_MAP
        #undef NEC
    #endif
};

static const tsNEC_Commands sNEC_CMD_1[] =
{
    #ifdef NEC_PROTOCOL_MAP
        #define NEC(Name, Cmd0, Cmd1, Cmd2) {Cmd1},
            NEC_PROTOCOL_MAP
        #undef NEC
    #endif
};

static const tsNEC_Commands sNEC_CMD_2[] =
{
    #ifdef NEC_PROTOCOL_MAP
        #define NEC(Name, Cmd0, Cmd1, Cmd2) {Cmd2},
            NEC_PROTOCOL_MAP
        #undef NEC
    #endif
};

/****************************************** Function prototypes ******************************************/
/****************************************** local functions *********************************************/

/****************************************** External visible functiones **********************************/
//********************************************************************************
/*!
\author     Kraemer E.
\date       01.02.2022
\brief      Gets the command according to the received address.
\return     eNecCmd - The NEC command
\param      ucCommand - The command as a value
\param      eNEC_Address - The NEC address to differ between the commands
***********************************************************************************/
teNEC_Commands IR_Commands_GetCommand(uint8_t ucCommand, teNEC_Address eNEC_Address)
{
    teNEC_Commands eNecCmd = eNEC_Cmd_Invalid;
    
    for(unsigned int cmdIdx = 0; cmdIdx < _countof(sNEC_CMD_0); cmdIdx++)
    {
        if(eNEC_Address == eNEC_Addr_0)
        {
            if(ucCommand == sNEC_CMD_0[cmdIdx].ucCommandCode)
            {
                eNecCmd = (teNEC_Commands)cmdIdx;
                break;
            }
        }
        else if(eNEC_Address == eNEC_Addr_1)
        {
            if(ucCommand == sNEC_CMD_1[cmdIdx].ucCommandCode)
            {
                eNecCmd = (teNEC_Commands)cmdIdx;
                break;
            }
        }
        else if(eNEC_Address == eNEC_Addr_2)
        {
            if(ucCommand == sNEC_CMD_2[cmdIdx].ucCommandCode)
            {
                eNecCmd = (teNEC_Commands)cmdIdx;
                break;
            }
        }
    }
    
    return eNecCmd;
}
